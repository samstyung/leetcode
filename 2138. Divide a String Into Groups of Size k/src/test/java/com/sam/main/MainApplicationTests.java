package com.sam.main;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainApplicationTests {

    @Test
    void test_abcdefghi_3_x() {
        String input = "abcdefghi";
        MainApplication app = new MainApplication();
        String[] result = app.divideString(input, 3, 'x');

        String[] expected = {"abc","def","ghi"};
        assertArrayEquals(expected, result);
    }

    @Test
    void test_abcdefghij_3_x() {
        String input = "abcdefghij";
        MainApplication app = new MainApplication();
        String[] result = app.divideString(input, 3, 'x');

        String[] expected = {"abc","def","ghi", "jxx"};
        assertArrayEquals(expected, result);
    }
}
