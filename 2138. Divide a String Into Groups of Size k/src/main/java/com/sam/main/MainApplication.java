package com.sam.main;

import java.util.LinkedList;
import java.util.List;

public class MainApplication {

	public String[] divideString(String s, int k, char fill) {

		if (s == null || s.length() == 0) {
			return new String[]{};
		}

		int numOfStr = (s.length()/k) + (s.length()%k > 0 ? 1:0);
		String[] list = new String[numOfStr];
		int listIndex = 0;
		int count = 0;
		String currentString = "";
		for (char c : s.toCharArray()) {
			currentString += c;

			count++;
			if (count == k) count = 0;

			if (count == 0) {
				list[listIndex] = currentString;
				listIndex++;
				currentString = "";
			}
		}

		while (count > 0 && count < k) {
			currentString += fill;
			count++;
			if (count == k) {
				list[listIndex] = currentString;
				break;
			}
		}

		return list;
	}


}
