package com.sam.main;

import static java.lang.Math.min;

public class MainApplication {
	public long maxRunTime(int n, int[] batteries) {
		long total = 0;
		long low = batteries[0];
		for (int b : batteries) {
			low = min(b, low);
			total += b;
		}

		long high = (total/n)+1;
		long ans = -1;
		while (low < high) {
			long mid = (low+high)/2;

			boolean canFit = canFit(n, mid, batteries);
			if (canFit) {
				ans = mid;
				low = mid+1;
			}
			else {
				high = mid;
			}
		}

		return ans;
	}

	private boolean canFit(int n, long time, int[] batteries) {
		long sum = 0;
		long targetSum = n*time;

		for (int b : batteries) {
			if (b < time) {
				sum += b;
			}
			else {
				sum += time;
			}

			if (sum >= targetSum) {
				return true;
			}
		}

		return false;
	}

	//Another slower solution
//	public long maxRunTime(int n, int[] batteries) {
//		if (n == 0 || batteries.length < n) {
//			return 0;
//		}
//
//		if (batteries.length == n) {
//			int[] sortedIndices = sortedIndices(Arrays.copyOf(batteries, batteries.length));
//			return batteries[sortedIndices[n-1]];
//		}
//
//		int time = 0;
//		while (true) {
//			System.out.println("time="+time);
//			System.out.println("n="+n+" batteries="+ Arrays.toString(batteries));
//
//			int[] sortedIndices = sortedIndices(Arrays.copyOf(batteries, batteries.length));
//			System.out.println("sortedIndices="+ Arrays.toString(sortedIndices));
//
//			int timeToLast = batteries[sortedIndices[n-1]] - batteries[sortedIndices[n]];
//			if (timeToLast == 0) {
//				timeToLast = 1;
//			}
//
//			for (int i=0; i<n; i++) {
//				if (batteries[sortedIndices[i]] == 0) return time;
//				batteries[sortedIndices[i]] -= timeToLast;
//			}
//
//			time += timeToLast;
//		}
//
//	}
//
//	int[] sortedIndices(int[] batteries) {
//		int[] indices = new int[batteries.length];
//		for (int i=0; i<indices.length; i++) {
//			indices[i] = i;
//		}
//
//		for (int i=0; i<batteries.length-1; i++) {
//			for (int j=i+1; j<batteries.length; j++) {
//				if (batteries[i] < batteries[j]) {
//					int temp = batteries[j];
//					batteries[j] = batteries[i];
//					batteries[i] = temp;
//
//					temp = indices[j];
//					indices[j] = indices[i];
//					indices[i] = temp;
//				}
//			}
//		}
//
//		return indices;
//	}

}
