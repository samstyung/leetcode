package com.sam.main;

import static java.lang.Math.max;

public class MainApplication {

	public long mostPoints(int[][] questions) {

		if (questions == null || questions.length == 0) {
			return 0;
		}

		if (questions.length == 1) {
			return questions[0][0];
		}

		long[] maxPoints = new long[questions.length];
		for (int i=questions.length-1; i >= 0 ; i--) {

			int[] q = questions[i];

			int points = q[0];
			int brainpower = q[1];

			long pointsForNotSkpping = (i+brainpower+1 > questions.length-1) ? points : points+maxPoints[i+brainpower+1];
			long pointsForSkipping = (i+1 > questions.length-1) ? 0 : maxPoints[i+1];
			maxPoints[i] = max(pointsForNotSkpping, pointsForSkipping);
		}

		return maxPoints[0];
	}

}
