package com.sam.main;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainApplicationTests {

    @Test
    void test_1() {
        int[][] questions = {{3,2},{4,3},{4,4},{2,5}};

        MainApplication app = new MainApplication();
        long result = app.mostPoints(questions);

        assertEquals(5, result);
    }

    @Test
    void test_2() {
        int[][] questions = {{1,1},{2,2},{3,3},{4,4},{5,5}};

        MainApplication app = new MainApplication();
        long result = app.mostPoints(questions);

        assertEquals(7, result);
    }

    @Test
    void test_3() {
        int[][] questions = {{12,46},{78,19},{63,15},{79,62},{13,10}};

        MainApplication app = new MainApplication();
        long result = app.mostPoints(questions);

        assertEquals(79, result);
    }


}
