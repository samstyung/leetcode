package com.sam.sum_of_root_to_leaf_binary_numbers;

public class SumOfRootToLeafBinaryNumbersApplication {

	public int sumRootToLeaf(TreeNode root) {
		if (root == null) {
			return 0;
		}

		// no branches at all
		if (root.left == null && root.right == null) {
			return root.val;
		}

		int leftSum = 0;
		int rightSum = 0;
		if (root.left != null) leftSum = sumRootToLeaf(root.left, root.val);
		if (root.right != null) rightSum = sumRootToLeaf(root.right, root.val);

		return leftSum + rightSum;
	}

	private int sumRootToLeaf(TreeNode root, int sum) {
		sum <<= 1;
		sum += root.val;

		// leaf
		if (root.left == null && root.right == null) {
			return sum;
		}

		// branch
		int leftSum = 0;
		int rightSum = 0;
		if (root.left != null) leftSum = sumRootToLeaf(root.left, sum);
		if (root.right != null) rightSum = sumRootToLeaf(root.right, sum);

		return leftSum + rightSum;
	}

}
