package com.sam.main;

import java.util.LinkedList;
import java.util.List;

public class MainApplication {

	public int minMoves(int target, int maxDoubles) {
		int steps = 0;

		while (target > 1) {
			if (target == 1) break;

			if (maxDoubles == 0) {
				steps += target-1;
				break;
			}

			if (target % 2 == 1) {
				target -= 1;
				steps++;
			}

			if (maxDoubles > 0) {
				target /= 2;
				maxDoubles--;
				steps++;
			}
		}

		return steps;
	}

}
