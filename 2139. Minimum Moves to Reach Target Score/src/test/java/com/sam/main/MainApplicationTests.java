package com.sam.main;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainApplicationTests {

    @Test
    void test_5_0() {
        MainApplication app = new MainApplication();
        int result = app.minMoves(5, 0);

        assertEquals(4, result);
    }

    @Test
    void test_19_2() {
        MainApplication app = new MainApplication();
        int result = app.minMoves(19, 2);

        assertEquals(7, result);
    }

    @Test
    void test_10_4() {
        MainApplication app = new MainApplication();
        int result = app.minMoves(10, 4);

        assertEquals(4, result);
    }

    @Test
    void test_656101987_1() {
        MainApplication app = new MainApplication();
        int result = app.minMoves(656101987, 1);

        assertEquals(328050994, result);
    }


}
